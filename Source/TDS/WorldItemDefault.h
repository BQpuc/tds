// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS/FuncLibrary/Types.h"
#include "WorldItemDefault.generated.h"

class UInventoryComponent;

UCLASS()
class TDS_API AWorldItemDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWorldItemDefault();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
  class USceneComponent* SceneComp = nullptr;
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
  class USphereComponent* SphereCollision = nullptr;
  UFUNCTION()
  void SphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
      int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
  UFUNCTION(BlueprintNativeEvent)
  void OverlapCharacter(UInventoryComponent* Inventory);
  void OverlapCharacter_Implementation(UInventoryComponent* Inventory);
  public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
      UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FWeaponSlot WeaponInfo;

  UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
  void InitActor(FDropItem InfoActor);
  void InitActor_Implementation(FDropItem InfoActor);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
  void PickUpSuccess();
  void PickUpSuccess_Implementation();
};
