// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDS/FuncLibrary/TDS_IGameActor.h"
#include "TDSCharacter.generated.h"

//class UInventoryComponent;
//class AWeaponDefault;

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public ITDS_IGameActor
{
  GENERATED_BODY()

  protected:
  virtual void BeginPlay() override;

  // ====== === Inputs === =======
  void InputAxisX(float Value);
  void InputAxisY(float Value);

  void InputAttackPressed();
  void InputAttackReleased();

  void InputWalkPressed();
  void InputWalkReleased();

  void InputSprintPressed();
  void InputSprintReleased();

  void InputAimPressed();
  void InputAimReleased();

  // Inventory Inputs
  // ������ ������ � ����� �� ���������/����������
  void TrySwitchNextWeapon();
  void TrySwitchPreviosWeapon();
  void InputKey1();
  void InputKey2();
  void InputKey3();
  void InputKey4();
  void InputKey6();
  void InputInteractPressed();
  void InputInteractReleased();

  // Ability Inputs
  // �������� ������
  void TryAbilityEnabled();

  template<int32 Id> void TKeyPressed() { TrySwitchToIndexByKeyInput(Id); }
  // =Inputs End=

  // Inputs Flags
  float AxisX = 0.0f;
  float AxisY = 0.0f;

  bool SprintEnabled = false;
  bool WalkEnabled = false;
  bool AimEnabled = false;
  bool bInteractKeyPressed = false;
  bool bIsAlive = true;

  EMovementState MovementState = EMovementState::RunState;
  UPROPERTY()
  class AWeaponDefault* CurrentWeapon = nullptr;
  UPROPERTY()
  UDecalComponent* CurrentCursor = nullptr;
  UPROPERTY()
  TArray<UTDS_StateEffect*> Effects;

  int32 CurrentIndexWeapon = 0;

  UFUNCTION()
  void CharDead();
  void EnableRagdoll();
  virtual float TakeDamage(
      float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

  public:
  ATDSCharacter();

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Inventory")
  class USphereComponent* InteractCollision;

  FTimerHandle TimerHandle_RagDollTimer;

  // Called every frame.
  virtual void Tick(float DeltaSeconds) override;

  virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

  /** Returns TopDownCameraComponent subobject **/
  FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
  /** Returns CameraBoom subobject **/
  FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
  class UInventoryComponent* InventoryComponent;
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
  class UTDS_HCChar* CharHealthComponent;
  
  // cursor
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
  UMaterialInterface* CursorMaterial = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
  FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

  // Default move rule and state character
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
  FCharacterSpeed MovementSpeedInfo;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
  TArray<UAnimMontage*> DeadsAnim;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
  TSubclassOf<UTDS_StateEffect> AbilityEffect;

  private:
  /** Top down camera */
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
  class UCameraComponent* TopDownCameraComponent;

  /** Camera boom positioning the camera above the character */
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
  class USpringArmComponent* CameraBoom;

  public:
  // tick Func
  UFUNCTION()
  void MovementTick(float DeltaTime);

  // Func
  // Posle izmeneniya MovementState menyaet skorost'(MaxWalkSpeed) personaja
  void CharacterUpdate();
  // Izmenenie sostoyaniya personaja. Vliyaet na MaxWalkSpeed
  void ChangeMovementState();

  void AttackCharEvent(bool bIsFiring);

  UFUNCTION() // ������ �������� ������
  void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
  // ����� InitReload � ������, ���� ������� ����������
  void TryReloadWeapon();
  UFUNCTION()
  void WeaponFireStart(UAnimMontage* Anim);
  UFUNCTION()
  void WeaponReloadStart(UAnimMontage* Anim);
  UFUNCTION()
  void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);

  bool TrySwitchToIndexByKeyInput(int32 ToIndex);
  void DropCurrentWeapon();

  UFUNCTION(BlueprintNativeEvent)
  void WeaponReloadStart_BP(UAnimMontage* Anim);
  void WeaponReloadStart_BP_Implementation(UAnimMontage* Anim);
  UFUNCTION(BlueprintNativeEvent)
  void WeaponReloadEnd_BP(bool bIsSuccess);
  void WeaponReloadEnd_BP_Implementation(bool bIsSuccess);
  UFUNCTION(BlueprintNativeEvent)
  void WeaponFireStart_BP(UAnimMontage* Anim);
  void WeaponFireStart_BP_Implementation(UAnimMontage* Anim);

  UFUNCTION(BlueprintCallable, BlueprintPure)
  class AWeaponDefault* GetCurrentWeapon() { return CurrentWeapon; }
  UFUNCTION(BlueprintCallable, BlueprintPure)
  UDecalComponent* GetCursorToWorld() { return CurrentCursor; }
  UFUNCTION(BlueprintCallable, BlueprintPure)
  EMovementState GetMovementState() { return MovementState; }
  UFUNCTION(BlueprintCallable, BlueprintPure)
  TArray<UTDS_StateEffect*> GetCurrentEffectOnChar() { return Effects; }
  UFUNCTION(BlueprintCallable, BlueprintPure)
  int32 GetCurrentWeaponIndex() { return CurrentIndexWeapon; }

    // ---Interface---
  TEnumAsByte<EPhysicalSurface> GetSurfaceType() override;
  TArray<UTDS_StateEffect*> GetAllCurrentEffects() override;
  void RemoveEffect(UTDS_StateEffect* RemoveEffect) override;
  void AddEffect(UTDS_StateEffect* newEffect) override;
  // ---End interface---


  protected:
  // My Variebles
    // for demo
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
  FName InitWeaponName;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
  float Direction = 0.0f;

  // Camera
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
  float HieghtCameraMin = 300.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
  float HieghtCameraMax = 1000.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
  float HieghtCameraChangeDistance = 50.0f;
  // Camera Aim
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
  float ClampedCameraHeight = 0.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
  float DistanceCameraBoomChar = 0.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
  float MinClampDistanceLogicCursorStart = 400.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
  float MaxClampDistanceLogicCursorStart = 300.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
  float SpeedChangeLocationCameraCursor = 15.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
  float SpeedReturnCameraDefaultPosition = 10.0f;

    FTimerHandle InteractPressedHandle;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interact")
  float PickUpTime = 1.f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interact")
  TSubclassOf<class AWorldItemDefault> PickUpClass = nullptr;
  // My Func:

  // Priblijenie i otdalenie camera
  UFUNCTION(BlueprintCallable)
  void CameraSlide(float AxisValue);

  // Camera move to cursor when aim
  UFUNCTION(BlueprintCallable)
  void CameraSlideToCursor(float DeltaTime);

  void TryPickUpActorPressed();
};
