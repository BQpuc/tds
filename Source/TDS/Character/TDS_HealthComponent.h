// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDS_HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);

/*USTRUCT(BlueprintType)
struct FStartParam
{
  GENERATED_USTRUCT_BODY()
};*/

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TDS_API UTDS_HealthComponent : public UActorComponent
{
  GENERATED_BODY()

  public:
  // Sets default values for this component's properties
  UTDS_HealthComponent();

  UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
  FOnHealthChange OnHealthChange;
  UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
  FOnDeath OnDead;

  protected:
  // Called when the game starts
  virtual void BeginPlay() override;

  float Health = 0.f;

  public:
  // Called every frame
  virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
  
      UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
      float CoefDamage = 1.0f;
      UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
      float MaxHealth = 99.f;

  UFUNCTION(BlueprintCallable, Category = "Health")
  float GetCurrentHealth() const;

  	UFUNCTION(BlueprintCallable, Category = "Health")
  void SetCurrentHealth(float NewHealth);
    UFUNCTION(BlueprintCallable, Category = "Health")
    virtual void ChangeHealthValue(float ChangeValue);

    /*
  UFUNCTION(BlueprintCallable, Category = "Health")
  virtual void ReceiveDamage(float Damage);

  UFUNCTION(BlueprintNativeEvent)
  void DeathEvent();
  void DeathEvent_Implementation();

  private:
  bool bIsDeath = false;
*/
};
