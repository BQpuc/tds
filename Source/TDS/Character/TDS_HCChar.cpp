// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HCChar.h"

void UTDS_HCChar::ChangeHealthValue(float ChangeValue)
{
  float CurrentDamage = ChangeValue * CoefDamage;

  if (Shield > 0.0f && ChangeValue < 0.0f)
  {
    ChangeShieldValue(ChangeValue);

    if (Shield < 0.0f)
    {
      // FX
      // UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
    }
  }
  else
  {
    Super::ChangeHealthValue(ChangeValue);
  }
}

float UTDS_HCChar::GetCurrentShield()
{
  return Shield;
}

void UTDS_HCChar::ChangeShieldValue(float ChangeValue)
{
  if (Health <= 0.f)
    return;

  Shield += ChangeValue;

  if (Shield > 100.0f)
  {
    Shield = 100.0f;
  }
  else
  {
    if (Shield < 0.0f)
      Shield = 0.0f;
  }

  if (GetWorld())
  {
    GetWorld()->GetTimerManager().SetTimer(
        TimerHandle_CollDownShieldTimer, this, &UTDS_HCChar::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

    GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
  }

  OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UTDS_HCChar::CoolDownShieldEnd()
{
  if (GetWorld() && Health > 0.f)
  {
    GetWorld()->GetTimerManager().SetTimer(
        TimerHandle_ShieldRecoveryRateTimer, this, &UTDS_HCChar::RecoveryShield, ShieldRecoverRate, true);
  }
}

void UTDS_HCChar::RecoveryShield()
{
  float tmp = Shield;
  tmp = tmp + ShieldRecoverValue;
  if (tmp > 100.0f)
  {
    Shield = 100.0f;
    if (GetWorld())
    {
      GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
    }
  }
  else
  {
    Shield = tmp;
  }

  OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}