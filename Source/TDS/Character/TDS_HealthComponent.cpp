// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Character/TDS_HealthComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UTDS_HealthComponent::UTDS_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDS_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	  Health = MaxHealth;
}


// Called every frame
void UTDS_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

 float UTDS_HealthComponent::GetCurrentHealth() const
{
  // if (!this) return 0.f;

  return Health;
}

 void UTDS_HealthComponent::SetCurrentHealth(float NewHealth)
{
   float ChangedHealth = 0.f;
   if (NewHealth <= 0.001f)
   {
     OnDead.Broadcast();
   }
   else if (NewHealth > MaxHealth)
   {
     ChangedHealth = MaxHealth - Health;
     Health = MaxHealth;
   }
   else if (NewHealth < MaxHealth && NewHealth > 0)
   {
     ChangedHealth = NewHealth - Health;
     Health = NewHealth;
   }
  
   OnHealthChange.Broadcast(Health, ChangedHealth);
   UE_LOG(LogTemp, Display, TEXT("HealthComponent::SetCurrentHealth"));
}

 void UTDS_HealthComponent::ChangeHealthValue(float ChangeValue)
{
   
  ChangeValue = ChangeValue * CoefDamage; // UKismetMathLibrary::RandomFloatInRange(CoefDamage * 0.9f, CoefDamage * 1.1f);
  float OldHealth = Health;
  Health += ChangeValue;

  if (Health < 0.0f)
  {
    ChangeValue = -OldHealth;
    OnHealthChange.Broadcast(Health, ChangeValue);
    OnDead.Broadcast();
    return;
  }
  if (Health > MaxHealth)
  {
    Health = MaxHealth;
  }
  OnHealthChange.Broadcast(Health, ChangeValue);
  //UE_LOG(LogTemp, Display, TEXT("HealthComponent::ChangeHealth Health %f"), Health);
}

 /*
void UTDS_HealthComponent::ReceiveDamage(float Damage)
{
  if (bIsDeath)
    return;

  Health -= Damage;
  OnHealthChange.Broadcast(Health, Damage);

  if (Health <= 0.f)
  {
    bIsDeath = true;
    DeathEvent();
    OnDeath.Broadcast();
  }
}

void UTDS_HealthComponent::DeathEvent_Implementation() 
{
//bp
}
*/
