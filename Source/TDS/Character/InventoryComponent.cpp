// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryComponent.h"
#include "TDS/Game/TDSGameInstance.h"
#include "TDS/FuncLibrary/TDS_IGameActor.h"
//#pragma optimize("", off)


// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
  PrimaryComponentTick.bCanEverTick = false;
}

void UInventoryComponent::BeginPlay()
{
  Super::BeginPlay();

  // Find init weaponsSlots and First Init Weapon
  for (int8 i = 0; i < WeaponSlots.Num(); i++)
  {
    UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
    if (myGI)
    {
      if (!WeaponSlots[i].NameItem.IsNone())
      { // ���������� ���������� �������� � ������� ������ � ���������
        FWeaponInfo Info;
        if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
          WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;
        else
        {
          // WeaponSlots.RemoveAt(i);
          // i--;
        }
      }
    }
  }
  MaxSlotsWeapon = WeaponSlots.Num();

  if (WeaponSlots.IsValidIndex(0))
  { // ����� � ���� ������ � �������� 0
    if (!WeaponSlots[0].NameItem.IsNone())
      OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
  } // ������� ���������� � TDSCharacter
}

/* Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
  Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

  // ...
}*/

bool UInventoryComponent::SwitchWeaponToIndexByNextPreviosIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
  bool bIsSuccess = false;
  int8 CorrectIndex = ChangeToIndex;
  if (ChangeToIndex > WeaponSlots.Num() - 1)
    CorrectIndex = 0; // ������������� �� ������ ������, ���� ������ ������ �������
  else if (ChangeToIndex < 0)
    CorrectIndex = WeaponSlots.Num() - 1; // ������������� �� ��������� ������, ���� ������ ������ 0

  FName NewIdWeapon;
  FAdditionalWeaponInfo NewAdditionalInfo;
  int32 NewCurrentIndex = 0;

  if (WeaponSlots.IsValidIndex(CorrectIndex))
  {
    if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
    {
      if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
      {
        // good weapon have ammo start change
        bIsSuccess = true;
      }
      else
      {
        UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
        if (myGI)
        {
          // check ammoSlots for this weapon
          FWeaponInfo myInfo;
          myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);

          bool bIsFind = false; // ���������� ��� ������ �� �����
          int8 j = 0;
          while (j < AmmoSlots.Num() && !bIsFind)
          {
            if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0) // ���������� ������ ���� ������
            {
              // good weapon have ammo start change
              bIsSuccess = true;
              bIsFind = true;
            }
            j++;
          }
        }
      }
      if (bIsSuccess) // ���� ������ �������
      {               // ���������� ��� �������� ����������
        NewCurrentIndex = CorrectIndex;
        NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;             // ���������� �������� ������
        NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo; // ���������� ������� ����� ������
      }
    }
  }

  if (!bIsSuccess) // ���� ������ �� �������
  {
      int8 iteration = 0;
      int8 Seconditeration = 0;
      int8 tmpIndex = 0;
      while (iteration < WeaponSlots.Num() && !bIsSuccess)
      {
        iteration++;
        if (bIsForward)
        {
          tmpIndex = ChangeToIndex + iteration;
        }
        else
        {
          Seconditeration = WeaponSlots.Num() - 1;
          tmpIndex = ChangeToIndex - iteration;
        }
        if (WeaponSlots.IsValidIndex(tmpIndex))
        {
          if (!WeaponSlots[tmpIndex].NameItem.IsNone())
          {
            if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
            {
              // WeaponGood
              bIsSuccess = true;
              NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
              NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
              NewCurrentIndex = tmpIndex;
            }
            else
            {
              FWeaponInfo myInfo;
              UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

              myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

              bool bIsFind = false;
              int8 j = 0;
              while (j < AmmoSlots.Num() && !bIsFind)
              {
                if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
                {
                  // WeaponGood
                  bIsSuccess = true;
                  NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
                  NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
                  NewCurrentIndex = tmpIndex;
                  bIsFind = true;
                }
                j++;
              }
            }
          }
        }
        else
        {
          // go to end of right of array weapon slots
          if (OldIndex != Seconditeration)
          {
            if (WeaponSlots.IsValidIndex(Seconditeration))
            {
              if (!WeaponSlots[Seconditeration].NameItem.IsNone())
              {
                if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
                {
                  // WeaponGood
                  bIsSuccess = true;
                  NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
                  NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
                  NewCurrentIndex = Seconditeration;
                }
                else
                {
                  FWeaponInfo myInfo;
                  UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

                  myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

                  bool bIsFind = false;
                  int8 j = 0;
                  while (j < AmmoSlots.Num() && !bIsFind)
                  {
                    if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
                    {
                      // WeaponGood
                      bIsSuccess = true;
                      NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
                      NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
                      NewCurrentIndex = Seconditeration;
                      bIsFind = true;
                    }
                    j++;
                  }
                }
              }
            }
          }
          else
          {
            // go to same weapon when start
            if (WeaponSlots.IsValidIndex(Seconditeration))
            {
              if (!WeaponSlots[Seconditeration].NameItem.IsNone())
              {
                if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
                {
                  // WeaponGood, it same weapon do nothing
                }
                else
                {
                  FWeaponInfo myInfo;
                  UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

                  myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

                  bool bIsFind = false;
                  int8 j = 0;
                  while (j < AmmoSlots.Num() && !bIsFind)
                  {
                    if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
                    {
                      if (AmmoSlots[j].Cout > 0)
                      {
                        // WeaponGood, it same weapon do nothing
                      }
                      else
                      {
                        // Not find weapon with amm need init Pistol with infinity ammo
                        UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
                      }
                    }
                    j++;
                  }
                }
              }
            }
          }
          if (bIsForward)
          {
            Seconditeration++;
          }
          else
          {
            Seconditeration--;
          }
        }
      }
  }  
   
  if (bIsSuccess)
  { // ��������� ������ ������ ������, ������� ��������
    SetAdditionalInfoWeapon(OldIndex, OldInfo);
    OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex); // �������� ���������, ��� ������ ���������
    // OnWeaponAmmoAviable.Broadcast()
  }

  return bIsSuccess;
}

bool UInventoryComponent::SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAdditionalWeaponInfo PreviosWeaponInfo)
{
  bool bIsSuccess = false;
  FName ToSwitchWeapon;
  FAdditionalWeaponInfo ToSwitchAdditionalInfo;

  ToSwitchWeapon = GetWeaponNameBySlotIndex(IndexWeaponToChange);
  ToSwitchAdditionalInfo = GetAdditionalInfoWeapon(IndexWeaponToChange);
  
  if (!ToSwitchWeapon.IsNone())
  {
    SetAdditionalInfoWeapon(PreviosIndex, PreviosWeaponInfo);
    OnSwitchWeapon.Broadcast(ToSwitchWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);

    EWeaponType ToSwitchWeaponType;
    if (GetWeaponTypeByNameWeapon(ToSwitchWeapon, ToSwitchWeaponType))
    {
      int8 AviableAmmoForWeapon = -1;
      if (CheckAmmoForWeapon(ToSwitchWeaponType, AviableAmmoForWeapon))
      {
      }
    }
    bIsSuccess = true;
  }

  return bIsSuccess;
}

FAdditionalWeaponInfo UInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
  FAdditionalWeaponInfo result;
  if (WeaponSlots.IsValidIndex(IndexWeapon))
  {
    bool bIsFind = false;
    int8 i = 0;
    while (i < WeaponSlots.Num() && !bIsFind)
    {
      if (i == IndexWeapon)
      {
        result = WeaponSlots[i].AdditionalInfo;
        bIsFind = true;
      }
      i++;
    }
    if (!bIsFind)
      UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
  }
  else
    UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);

  return result;
}

int32 UInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
  int32 result = -1;
  int8 i = 0;
  bool bIsFind = false;
  while (i < WeaponSlots.Num() && !bIsFind)
  {
    if (WeaponSlots[i].NameItem == IdWeaponName)
    {
      bIsFind = true;
      result = i;
    }
    i++;
  }
  return result;
}

FName UInventoryComponent::GetWeaponNameBySlotIndex(int32 indexSlot)
{
  FName result;

  if (WeaponSlots.IsValidIndex(indexSlot))
  {
    result = WeaponSlots[indexSlot].NameItem;
  }
  return result;
}

bool UInventoryComponent::GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType)
{
  bool bIsFind = false;
  FWeaponInfo OutInfo;
  WeaponType = EWeaponType::RifleType;
  UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
  if (myGI && WeaponSlots.IsValidIndex(IndexSlot))
  {
    myGI->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
    WeaponType = OutInfo.WeaponType;
    bIsFind = true;
  }
  return false;
}

bool UInventoryComponent::GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType)
{
  bool bIsFind = false;
  FWeaponInfo OutInfo;
  WeaponType = EWeaponType::RifleType;
  UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
  if (myGI)
  {
    myGI->GetWeaponInfoByName(IdWeaponName, OutInfo);
    WeaponType = OutInfo.WeaponType;
    bIsFind = true;
  }
  return bIsFind;
}

void UInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
  if (!WeaponSlots.IsValidIndex(IndexWeapon))
  { // ��������� ��� ������ �������
    UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
    return;
  }
  bool bIsFind = false;
  int8 i = 0;
  while (i < WeaponSlots.Num() && !bIsFind)
  {  // ���� ������ ������
    if (i == IndexWeapon)
    { // ���������� ������� ��� ���������
      WeaponSlots[i].AdditionalInfo = NewInfo;
      bIsFind = true;
      // �������� ��������� ��� ���������� �������� ����������
      OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
    }
    i++;
  }
  if (!bIsFind)
    UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
}

void UInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
  bool bIsFind = false;
  int8 i = 0;
  while (i < AmmoSlots.Num() && !bIsFind)
  {
    if (AmmoSlots[i].WeaponType == TypeWeapon)
    {
      AmmoSlots[i].Cout += CoutChangeAmmo;
      if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
        AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;

      OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);

      bIsFind = true;
    }
    i++;
  }
}

bool UInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8& AviableAmmForWeapon)
{
  AviableAmmForWeapon = 0;
  bool bIsFind = false;
  int8 i = 0;
  while (i < AmmoSlots.Num() && !bIsFind)
  {
    if (AmmoSlots[i].WeaponType == TypeWeapon)
    {
      bIsFind = true;
      AviableAmmForWeapon = AmmoSlots[i].Cout;
      if (AmmoSlots[i].Cout > 0)
      {
        return true;
      }
    }
    i++;
  }

  if (AviableAmmForWeapon <= 0)
  {
    OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
  }
  else
  {
    OnWeaponAmmoAviable.Broadcast(TypeWeapon);
  }

  return false;
}

bool UInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
  bool result = false;
  int8 i = 0;
  while (i < AmmoSlots.Num() && !result)
  {
    if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
      result = true;
    i++;
  }
  return result;
}

bool UInventoryComponent::CheckCanTakeWeapon(int32& FreeSlot)
{
  bool bIsFreeSlot = false;
  int8 i = 0;
  while (i < WeaponSlots.Num() && !bIsFreeSlot)
  {
    if (WeaponSlots[i].NameItem.IsNone())
    {
      bIsFreeSlot = true;
      FreeSlot = i;
    }
    i++;
  }
  return bIsFreeSlot;
}

bool UInventoryComponent::SwitchWeaponToInventory(
    FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo)
{
  bool result = false;

  if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
  {
    WeaponSlots[IndexSlot] = NewWeapon;

    SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeaponChar, -1, NewWeapon.AdditionalInfo, true);

    OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
    result = true;
  }
  return result;
}

bool UInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
  int32 indexSlot = -1;
  if (CheckCanTakeWeapon(indexSlot))
  {
    if (WeaponSlots.IsValidIndex(indexSlot))
    {
      WeaponSlots[indexSlot] = NewWeapon;

      OnUpdateWeaponSlots.Broadcast(indexSlot, NewWeapon);
      return true;
    }
  }
  return false;
}

void UInventoryComponent::DropWeaponByIndex(int32 Index, FDropItem& DropItemInfo) 
{
  FWeaponSlot EmptyWeaponSlot;

  bool bIsCanDrop = false;
  int8 AviableWeaponNum = 0;
  for (int8 i = 0; i < WeaponSlots.Num() && !bIsCanDrop; i++)
  {
    if (!WeaponSlots[i].NameItem.IsNone())
    {
      AviableWeaponNum++;
      if (AviableWeaponNum > 1)
        bIsCanDrop = true;
    }
  }
  if (bIsCanDrop && WeaponSlots.IsValidIndex(Index) && GetDropItemInfoFromInventory(Index, DropItemInfo))
  {
    GetDropItemInfoFromInventory(Index, DropItemInfo);

    bool bIsFindWeapon = false;
    for (int8 i = 0; i < WeaponSlots.Num() && !bIsFindWeapon; i++)
    {
      if (!WeaponSlots[i].NameItem.IsNone())
      {
        OnSwitchWeapon.Broadcast(WeaponSlots[i].NameItem, WeaponSlots[i].AdditionalInfo, i);
      }
    }

    WeaponSlots[Index] = EmptyWeaponSlot;
    if (GetOwner() && GetOwner()->GetClass() && GetOwner()->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass()))
    {
      ITDS_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
    }
    OnUpdateWeaponSlots.Broadcast(Index, EmptyWeaponSlot);
  }
}

bool UInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem& DropItemInfo)
{
  bool result = false;

  FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

  UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
  if (myGI)
  {
    result = myGI->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
    if (WeaponSlots.IsValidIndex(IndexSlot))
    {
      DropItemInfo.AdditionalWeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
    }
  }

  return result;
}

//#pragma optimize("", on)