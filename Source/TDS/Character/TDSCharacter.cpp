// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDS/Game/TDSGameInstance.h"
#include "TDS/Character/InventoryComponent.h"
#include "TDS/Weapons/WeaponDefault.h"
#include "Engine/World.h"
#include "TDS/Character/TDS_HCChar.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TDS/Weapons/Projectiles/ProjectileDefault.h"
#include "Components/SphereComponent.h"
#include "TDS/WorldItemDefault.h"

DEFINE_LOG_CATEGORY_STATIC(LogCharacter, All, All)

ATDSCharacter::ATDSCharacter()
{
  // Set size for player capsule
  GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

  // Don't rotate character to camera direction
  bUseControllerRotationPitch = false;
  bUseControllerRotationYaw = false;
  bUseControllerRotationRoll = false;

  // Configure character movement
  GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
  GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
  GetCharacterMovement()->bConstrainToPlane = true;
  GetCharacterMovement()->bSnapToPlaneAtStart = true;

  // Create a camera boom...
  CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
  CameraBoom->SetupAttachment(RootComponent);
  CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
  CameraBoom->TargetArmLength = 800.f;
  CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
  CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

  // Create a camera...
  TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
  TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
  TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

  
  InteractCollision = CreateDefaultSubobject<USphereComponent>(TEXT("InteractCollision"));
  InteractCollision->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
  InteractCollision->SetupAttachment(GetRootComponent());

  InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
  if (InventoryComponent)
  {
    InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDSCharacter::InitWeapon);
  }

  CharHealthComponent = CreateDefaultSubobject<UTDS_HCChar>(TEXT("HealthComponent"));
  if (CharHealthComponent)
  {
    CharHealthComponent->OnDead.AddDynamic(this, &ATDSCharacter::CharDead);
  }


  // Activate ticking in order to update the cursor every frame.
  PrimaryActorTick.bCanEverTick = true;
  PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::BeginPlay()
{
  Super::BeginPlay();

  if (CursorMaterial)
  {
    CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
  }
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
  Super::Tick(DeltaSeconds);

  if (CurrentCursor)
  {
    APlayerController* myPC = Cast<APlayerController>(GetController());
    if (myPC)
    {
      FHitResult TraceHitResult;
      myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
      FVector CursorFV = TraceHitResult.ImpactNormal;
      FRotator CursorR = CursorFV.Rotation();

      CurrentCursor->SetWorldLocation(TraceHitResult.Location);
      CurrentCursor->SetWorldRotation(CursorR);
    }
  }

  MovementTick(DeltaSeconds);

  // My logic. Sprint only to front
  if (SprintEnabled && (Direction > 5.0f || Direction < -5.0f))
  {
    SprintEnabled = false;
    ChangeMovementState();
  }
  CameraSlideToCursor(DeltaSeconds);
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
  Super::SetupPlayerInputComponent(NewInputComponent);

  NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
  NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);

  NewInputComponent->BindAction(TEXT("ChangeToSprint"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputSprintPressed);
  NewInputComponent->BindAction(TEXT("ChangeToSprint"), EInputEvent::IE_Released, this, &ATDSCharacter::InputSprintReleased);
  NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputWalkPressed);
  NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Released, this, &ATDSCharacter::InputWalkReleased);
  NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAimPressed);
  NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAimReleased);

  NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
  NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAttackReleased);
  NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::TryReloadWeapon);
 
  NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwitchNextWeapon);
  NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwitchPreviosWeapon);
  NewInputComponent->BindAction(TEXT("ActionNum1"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputKey1);
  NewInputComponent->BindAction(TEXT("ActionNum2"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputKey2);
  NewInputComponent->BindAction(TEXT("ActionNum3"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputKey3);
  NewInputComponent->BindAction(TEXT("ActionNum4"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputKey4);
  NewInputComponent->BindAction("AbilityAction", EInputEvent::IE_Pressed, this, &ATDSCharacter::TryAbilityEnabled);
  NewInputComponent->BindAction(TEXT("ActionNum6"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputKey6);
  NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::DropCurrentWeapon);
  NewInputComponent->BindAction(TEXT("InteractPickUpActor"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputInteractPressed);
  NewInputComponent->BindAction(TEXT("InteractPickUpActor"), EInputEvent::IE_Released, this, &ATDSCharacter::InputInteractReleased);

  
  /*
  TArray<FKey> HotKeys;
  HotKeys.Add(EKeys::One);
  HotKeys.Add(EKeys::Two);
  HotKeys.Add(EKeys::Three);
  HotKeys.Add(EKeys::Four);
  HotKeys.Add(EKeys::Five);
  HotKeys.Add(EKeys::Six);
  HotKeys.Add(EKeys::Seven);
  HotKeys.Add(EKeys::Eight);
  HotKeys.Add(EKeys::Nine);
  HotKeys.Add(EKeys::Zero);

  NewInputComponent->BindKey(HotKeys[1], EInputEvent::IE_Pressed, this, &ATDSCharacter::TKeyPressed<1>);
  NewInputComponent->BindKey(HotKeys[2], EInputEvent::IE_Pressed, this, &ATDSCharacter::TKeyPressed<2>);
  NewInputComponent->BindKey(HotKeys[3], EInputEvent::IE_Pressed, this, &ATDSCharacter::TKeyPressed<3>);
  NewInputComponent->BindKey(HotKeys[4], EInputEvent::IE_Pressed, this, &ATDSCharacter::TKeyPressed<4>);
  NewInputComponent->BindKey(HotKeys[5], EInputEvent::IE_Pressed, this, &ATDSCharacter::TKeyPressed<5>);
  NewInputComponent->BindKey(HotKeys[6], EInputEvent::IE_Pressed, this, &ATDSCharacter::TKeyPressed<6>);
  NewInputComponent->BindKey(HotKeys[7], EInputEvent::IE_Pressed, this, &ATDSCharacter::TKeyPressed<7>);
  NewInputComponent->BindKey(HotKeys[8], EInputEvent::IE_Pressed, this, &ATDSCharacter::TKeyPressed<8>);
  NewInputComponent->BindKey(HotKeys[9], EInputEvent::IE_Pressed, this, &ATDSCharacter::TKeyPressed<9>);
  NewInputComponent->BindKey(HotKeys[0], EInputEvent::IE_Pressed, this, &ATDSCharacter::TKeyPressed<0>);*/

}

void ATDSCharacter::InputAttackPressed()
{
  AttackCharEvent(true);
}

void ATDSCharacter::InputAttackReleased()
{
  AttackCharEvent(false);
}

void ATDSCharacter::InputWalkPressed()
{
  WalkEnabled = true;
  ChangeMovementState();
}

void ATDSCharacter::InputWalkReleased()
{
  WalkEnabled = false;
  ChangeMovementState();
}

void ATDSCharacter::InputSprintPressed()
{
  SprintEnabled = true;
  ChangeMovementState();
}

void ATDSCharacter::InputSprintReleased()
{
  SprintEnabled = false;
  ChangeMovementState();
}

void ATDSCharacter::InputAimPressed()
{
  AimEnabled = true;
  ChangeMovementState();
}

void ATDSCharacter::InputAimReleased()
{
  AimEnabled = false;
  ChangeMovementState();
}

void ATDSCharacter::InputAxisX(float Value)
{
  AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
  AxisY = Value;
}

/*Rotation Character on Cursor*/
void ATDSCharacter::MovementTick(float DeltaTime)
{
  if (!bIsAlive)
    return;

  AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
  AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

  if (MovementState == EMovementState::Sprint_State)
  {
    FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
    FRotator myRotator = myRotationVector.ToOrientationRotator();
    SetActorRotation(FQuat(myRotator));
  }
  else
  {
    APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
    if (myController)
    {
      FHitResult ResultHit;

      // Cursor ignore collision "LandscapeCursor" Config\DefaultEngine
      myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

      float RotatYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
      SetActorRotation(FQuat(FRotator(0.0f, RotatYaw, 0.0f)));

      if (CurrentWeapon)
      {
        //float Displacement = 0.0f;
        FVector Displacement = FVector(0);
        switch (MovementState)
        {
        case EMovementState::Aim_State:
          //Displacement = CurrentCursor->GetComponentLocation().Z + CurrentWeapon->WeaponSetting.AnimWeaponInfo.DispAimHigh;
          Displacement = FVector(0.0f, 0.0f, 160.0f);
          CurrentWeapon->ShouldReduceDispersion = true;
            break;
        case EMovementState::AimWalk_State:
          //Displacement = CurrentCursor->GetComponentLocation().Z + CurrentWeapon->WeaponSetting.AnimWeaponInfo.DispAimHigh;
          CurrentWeapon->ShouldReduceDispersion = true;
          Displacement = FVector(0.0f, 0.0f, 160.0f);
            break;
        case EMovementState::Walk_State:
          //Displacement = CurrentCursor->GetComponentLocation().Z + CurrentWeapon->WeaponSetting.AnimWeaponInfo.DisplHigh;
          Displacement = FVector(0.0f, 0.0f, 120.0f);
          CurrentWeapon->ShouldReduceDispersion = false;
            break;
        case EMovementState::RunState:
          //Displacement = CurrentCursor->GetComponentLocation().Z + CurrentWeapon->WeaponSetting.AnimWeaponInfo.DisplHigh;
          Displacement = FVector(0.0f, 0.0f, 120.0f);
          CurrentWeapon->ShouldReduceDispersion = false;
            break;
        case EMovementState::Sprint_State:
          break;
        default:
          break;
        }
        //CurrentWeapon->ShootEndLocation = FVector(ResultHit.Location.X, ResultHit.Location.Y, Displacement);
        // aimCursor like 3d Widget&
        CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
      }
    }
  }
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
  AWeaponDefault* myWeapon = nullptr;
  myWeapon = GetCurrentWeapon();
  if (myWeapon)
  {
    // ToDo Check melee or range
    myWeapon->SetWeaponStateFire(bIsFiring);
  }
  else
  {
    UE_LOG(LogCharacter, Warning, TEXT("ATDSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
  }
}

void ATDSCharacter::CharacterUpdate()
{
  float ResSpeed = 600.0f;
  switch (MovementState)
  {
  case EMovementState::AimWalk_State:
    ResSpeed = MovementSpeedInfo.AimSpeedWalk;
    break;
  case EMovementState::Aim_State:
    ResSpeed = MovementSpeedInfo.AimSpeed;
    break;
  case EMovementState::Walk_State:
    ResSpeed = MovementSpeedInfo.WalkSpeed;
    break;
  case EMovementState::RunState:
    ResSpeed = MovementSpeedInfo.RunSpeed;
    break;
  case EMovementState::Sprint_State:
    ResSpeed = MovementSpeedInfo.SprintSpeed;
    break;
  default:
    break;
  }
  GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
  if (SprintEnabled && Direction > -5.0f && Direction < 5.0f)
  {
    WalkEnabled = false;
    AimEnabled = false;
    MovementState = EMovementState::Sprint_State;
  }
  else if (WalkEnabled && !SprintEnabled && AimEnabled)
  {
    MovementState = EMovementState::AimWalk_State;
  }
  else if (WalkEnabled && !SprintEnabled && !AimEnabled)
  {
    MovementState = EMovementState::Walk_State;
  }
  else if (!WalkEnabled && !SprintEnabled && AimEnabled)
  {
    MovementState = EMovementState::Aim_State;
  }
  else if (!WalkEnabled && !SprintEnabled && !AimEnabled)
  {
    MovementState = EMovementState::RunState;
  }

  CharacterUpdate();

  AWeaponDefault* myWeapon = GetCurrentWeapon();
  if (myWeapon)
  {
    myWeapon->UpdateStateWeapon(MovementState);
  }
}

void ATDSCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
  if (CurrentWeapon)
  { // ���� ���� ������ � �����, ��� ������������ 
    CurrentWeapon->Destroy();
    CurrentWeapon = nullptr;
  }

  UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
  FWeaponInfo myWeaponInfo;
  if (!myGI)
  {
    UE_LOG(LogCharacter, Error, TEXT("Set GameInstance in Project Settings - Maps & Modes"));
    return;
  }

  bool InfoByName = myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo);
  if (!InfoByName)
  {
    UE_LOG(LogCharacter, Warning, TEXT("ATDSCharacter::InitWeapon - Weapon not found in DataTable -NULL"));
    return;
  }
  if (!myWeaponInfo.WeaponClass)
  {
    UE_LOG(LogCharacter, Warning, TEXT("myWeaponInfo.WeaponClass = NULL"))
    return;
  }

  FVector SpawnLocation = FVector(0);
  FRotator SpawnRotation = FRotator(0);

  FActorSpawnParameters SpawnParams;
  SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
  SpawnParams.Owner = this;
  SpawnParams.Instigator = GetInstigator();

  AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(
          myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
  if (!myWeapon)
    return;

  FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
  myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
  CurrentWeapon = myWeapon;

  myWeapon->WeaponSetting = myWeaponInfo;
  // myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;

  myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
  myWeapon->UpdateStateWeapon(MovementState);

  myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
  // if(InventoryComponent)
  CurrentIndexWeapon = NewCurrentIndexWeapon; // fix

  myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
  myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);
  myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDSCharacter::WeaponFireStart);

  // after switch try reload weapon if needed
  if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
    CurrentWeapon->InitReload();

  if (InventoryComponent)
    InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
}

void ATDSCharacter::TryReloadWeapon()
{
  if (!CurrentWeapon)
    return;
  if (CurrentWeapon->WeaponReloading)
    return;
  if (CurrentWeapon->GetWeaponRound() >= CurrentWeapon->WeaponSetting.MaxRound)
    return;
  if (!CurrentWeapon->CheckCanWeaponReload())
    return;

  CurrentWeapon->InitReload();
}

void ATDSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
  WeaponReloadStart_BP(Anim);
}

void ATDSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe)
{
  if (InventoryComponent && CurrentWeapon)
  {
    InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
    InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
  }
  WeaponReloadEnd_BP(bIsSuccess);
}

bool ATDSCharacter::TrySwitchToIndexByKeyInput(int32 ToIndex)
{
  if (!CurrentWeapon)
    return false;
  if (CurrentWeapon->WeaponReloading)
    return false;
  if (!InventoryComponent)
    return false;
  if (!InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
    return false;
  if (CurrentIndexWeapon == ToIndex)
    return false;

  if (CurrentWeapon->WeaponReloading)
    CurrentWeapon->CancelReload();
  int32 OldIndex = CurrentIndexWeapon;
  FAdditionalWeaponInfo OldInfo = CurrentWeapon->AdditionalWeaponInfo;
  return InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
}

void ATDSCharacter::DropCurrentWeapon()
{
  if (!InventoryComponent)
    return;
  FDropItem ItemInfo;
  InventoryComponent->DropWeaponByIndex(CurrentIndexWeapon, ItemInfo);
}

void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
  // in BP
}

void ATDSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
  // in BP
}

// Skillbox 2: 7.4 Homework WeaponSystem
void ATDSCharacter::WeaponFireStart(UAnimMontage* Anim)
{
  if (InventoryComponent && CurrentWeapon)
    InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
  WeaponFireStart_BP(Anim);
}

void ATDSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
  // in BP
}

void ATDSCharacter::TrySwitchNextWeapon()
{
  if (InventoryComponent->WeaponSlots.Num() <= 1)
    return;
  if (!CurrentWeapon)
    return;
  if (CurrentWeapon->WeaponReloading)
    return;

    // We have more then one weapon go switch
  int8 OldIndex = CurrentIndexWeapon;
  FAdditionalWeaponInfo OldInfo;
  if (CurrentWeapon)
  {
    OldInfo = CurrentWeapon->AdditionalWeaponInfo;
    if (CurrentWeapon->WeaponReloading)
      CurrentWeapon->CancelReload();
  }
  if (InventoryComponent)
    InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true);
}

void ATDSCharacter::TrySwitchPreviosWeapon()
{
  if (InventoryComponent->WeaponSlots.Num() <= 1)
    return;
  if (!CurrentWeapon)
    return;
  if (CurrentWeapon->WeaponReloading)
    return;

  // We have more then one weapon go switch
  int8 OldIndex = CurrentIndexWeapon;
  FAdditionalWeaponInfo OldInfo;
  if (CurrentWeapon)
  {
    OldInfo = CurrentWeapon->AdditionalWeaponInfo;
    if (CurrentWeapon->WeaponReloading)
      CurrentWeapon->CancelReload();
  }

  if (InventoryComponent)
  {
    // InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
    InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false);
  }
}

void ATDSCharacter::InputKey1()
{
  TrySwitchToIndexByKeyInput(0);
}

void ATDSCharacter::InputKey2()
{
  TrySwitchToIndexByKeyInput(1);
}

void ATDSCharacter::InputKey3()
{
  TrySwitchToIndexByKeyInput(2);
}

void ATDSCharacter::InputKey4()
{
  TrySwitchToIndexByKeyInput(3);
}

void ATDSCharacter::InputKey6()
{
  TrySwitchToIndexByKeyInput(4);
}

void ATDSCharacter::InputInteractPressed()
{
  bInteractKeyPressed = true;
  if (InteractPressedHandle.IsValid())
    GetWorldTimerManager().ClearTimer(InteractPressedHandle);
  GetWorldTimerManager().SetTimer(InteractPressedHandle, this, &ATDSCharacter::TryPickUpActorPressed, PickUpTime);
}

void ATDSCharacter::InputInteractReleased()
{
  bInteractKeyPressed = false;
  if (InteractPressedHandle.IsValid())
    GetWorldTimerManager().ClearTimer(InteractPressedHandle);
}

void ATDSCharacter::TryAbilityEnabled()
{
  if (!AbilityEffect)
    return;

  UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(this, AbilityEffect);
  if (NewEffect)
  {
    NewEffect->InitObject(this);
  }
}

TEnumAsByte<EPhysicalSurface> ATDSCharacter::GetSurfaceType()
{
  EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
  if (!CharHealthComponent && CharHealthComponent->GetCurrentShield() > 0.f)
    return Result;
  if (!GetMesh() && !GetMesh()->GetMaterial(0))
    return Result;

  UPhysicalMaterial* myPMat = GetMesh()->GetMaterial(0)->GetPhysicalMaterial();
  if (myPMat)
    Result = myPMat->SurfaceType;

  return Result;
}

TArray<UTDS_StateEffect*> ATDSCharacter::GetAllCurrentEffects()
{
  return Effects;
}

void ATDSCharacter::RemoveEffect(UTDS_StateEffect* RemoveEffect)
{
  Effects.Remove(RemoveEffect);
}

void ATDSCharacter::AddEffect(UTDS_StateEffect* newEffect)
{
  Effects.Add(newEffect);
}

void ATDSCharacter::CharDead()
{
  float TimeAnim = 0.0f;
  int32 rnd = FMath::RandHelper(DeadsAnim.Num());
  if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
  {
    TimeAnim = DeadsAnim[rnd]->GetPlayLength();
    GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
  }

  bIsAlive = false;

  UnPossessed();

  // Timer rag doll
  GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATDSCharacter::EnableRagdoll, TimeAnim, false);

  GetCursorToWorld()->SetVisibility(false);
}

void ATDSCharacter::EnableRagdoll()
{
  if (!GetMesh())
    return;

  GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
  GetMesh()->SetSimulatePhysics(true);
}

float ATDSCharacter::TakeDamage(
    float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
  float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
  if (bIsAlive)
  {
    CharHealthComponent->ChangeHealthValue(-DamageAmount);
  }
  if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
  {
    AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
    if (myProjectile)
    {
      UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
    }
  }

  return ActualDamage;
}



// Skillbox 2: 2.4 Character, Camera
void ATDSCharacter::CameraSlide(float AxisValue)
{
  if (AxisValue < 0.0f)
  {
    float PlusDistance = CameraBoom->TargetArmLength + HieghtCameraChangeDistance;
    if (HieghtCameraMax >= PlusDistance)
    {
      CameraBoom->TargetArmLength = PlusDistance;
    }
  }
  else if (AxisValue > 0.0f)
  {
    float MinusDistance = CameraBoom->TargetArmLength - HieghtCameraChangeDistance;
    if (HieghtCameraMin <= MinusDistance)
    {
      CameraBoom->TargetArmLength = MinusDistance;
    }
  }
}

// Skillbox 2: 6.3 Aiming
void ATDSCharacter::CameraSlideToCursor(float DeltaTime)
{
  ClampedCameraHeight = UKismetMathLibrary::MapRangeClamped(CameraBoom->TargetArmLength, HieghtCameraMin, HieghtCameraMax, 0.5f, 1.0f);

  FVector LCursorLoc = GetCursorToWorld()->GetComponentLocation();
  FVector LCapsuleLoc = GetCapsuleComponent()->GetComponentLocation();
  FVector LDistanceCamLoc = LCapsuleLoc - LCursorLoc;
  LDistanceCamLoc.Z = 0.0f;
  DistanceCameraBoomChar = LDistanceCamLoc.Size();

  bool IsAim = ((MovementState == EMovementState::Aim_State) || (MovementState == EMovementState::AimWalk_State));
  if (IsAim)
  {
    bool IsFarAway = DistanceCameraBoomChar > (ClampedCameraHeight * MinClampDistanceLogicCursorStart);

    if (IsFarAway)
    {
      FVector LIsFarAwayLoc = (CameraBoom->GetComponentLocation() - LCapsuleLoc);
      LIsFarAwayLoc.Z = 0.0f;
      float LLength3 = LIsFarAwayLoc.Size();

      if (LLength3 > (ClampedCameraHeight * MaxClampDistanceLogicCursorStart))
      {
      }
      else
      {
        FRotator LocRotator =
            UKismetMathLibrary::FindLookAtRotation(GetCapsuleComponent()->GetComponentLocation(), CurrentCursor->GetComponentLocation());

        FVector2D ThoDVector = UKismetMathLibrary::Conv_VectorToVector2D(UKismetMathLibrary::GetForwardVector(LocRotator));

        UKismetMathLibrary::Normalize2D(ThoDVector);

        ThoDVector.X = ThoDVector.X * SpeedChangeLocationCameraCursor;
        ThoDVector.Y = ThoDVector.Y * SpeedChangeLocationCameraCursor;

        FVector NewLoc = FVector(0.0f, 0.0f, 0.0f);

        NewLoc.X = (ThoDVector.X + CameraBoom->GetComponentLocation().X);
        NewLoc.Y = (ThoDVector.Y + CameraBoom->GetComponentLocation().Y);
        NewLoc.Z = (CameraBoom->GetComponentLocation().Z);

        CameraBoom->SetWorldLocation(NewLoc);
      }
    }
    else
    {
      FVector NewLoc = UKismetMathLibrary::VInterpTo(
          CameraBoom->GetComponentLocation(), GetCapsuleComponent()->GetComponentLocation(), DeltaTime, SpeedReturnCameraDefaultPosition);

      CameraBoom->SetWorldLocation(NewLoc);
    }
  }
  else
  {
    FVector NewLoc = UKismetMathLibrary::VInterpTo(
        CameraBoom->GetComponentLocation(), GetCapsuleComponent()->GetComponentLocation(), DeltaTime, SpeedReturnCameraDefaultPosition);

    CameraBoom->SetWorldLocation(NewLoc);
  }
}

void ATDSCharacter::TryPickUpActorPressed()
{
  if (!PickUpClass)
  {
    UE_LOG(LogCharacter, Error, TEXT("3anoLHuTe PickUpClass. Category Interact"));
    return;
  }

  TArray<AActor*> Actors;
  InteractCollision->GetOverlappingActors(Actors, PickUpClass);
  if (!Actors.IsValidIndex(0))
  {
    UE_LOG(LogCharacter, Warning, TEXT("Array Actors is Empty"));
    return;
  }

   UE_LOG(LogCharacter, Display, TEXT("Actor %s Overlap"), *Actors[0]->GetName());
  AWorldItemDefault* PickUp = Cast<AWorldItemDefault>(Actors[0]);
  if (!PickUp)
    return;
  if (InventoryComponent->TryGetWeaponToInventory(PickUp->WeaponInfo))
    PickUp->PickUpSuccess();
}



