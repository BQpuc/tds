// Fill out your copyright notice in the Description page of Project Settings.

#include "TDSGameInstance.h"
#include "Engine/DataTable.h"
#include "TDS/Weapons/WeaponDefault.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
  bool bIsFind = false;
  FWeaponInfo* WeaponInfoRow;
  if (WeaponInfoTable)
  {
    FString ContextString;
    WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, ContextString, false);
    if (WeaponInfoRow)
    {
      bIsFind = true;
      OutInfo = *WeaponInfoRow;
    }
  }
  else
  {
    UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstanceInfoByName - WeaponTable -NULL"));
  }

  return bIsFind;
}

bool UTDSGameInstance::GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo)
{
  bool bIsFind = false;

  if (DropItemInfoTable)
  {
    FDropItem* DropItemInfoRow;
    TArray<FName> RowNames = DropItemInfoTable->GetRowNames();

    int8 i = 0;
    while (i < RowNames.Num() && !bIsFind)
    {
      DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
      if (DropItemInfoRow->AdditionalWeaponInfo.NameItem == NameItem)
      {
        OutInfo = (*DropItemInfoRow);
        bIsFind = true;
      }
      i++; // fix
    }
  }
  else
  {
    UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetDropItemInfoByWeaponName - DropItemInfoTable -NULL"));
  }

  return bIsFind;
}

bool UTDSGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
  bool bIsFind = false;
  FDropItem* DropWeaponInfoRow;
  if (WeaponInfoTable)
  {
    DropWeaponInfoRow = WeaponInfoTable->FindRow<FDropItem>(NameItem, "", false);
    if (DropWeaponInfoRow)
    {
      bIsFind = true;
      OutInfo = *DropWeaponInfoRow;
    }
  }
  else
  {
    UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetDropItemInfoByName - WeaponTable -NULL"));
  }

  return bIsFind;
}
