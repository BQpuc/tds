// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TDS/FuncLibrary/Types.h"

#include "TDSGameInstance.generated.h"

class UDataTable;

UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
  GENERATED_BODY()

  public:
  // table
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
  UDataTable* WeaponInfoTable = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " WeaponSetting ")
  UDataTable* DropItemInfoTable = nullptr;

  UFUNCTION(BlueprintCallable, BlueprintPure)
  bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);

  UFUNCTION(BlueprintCallable)
  bool GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo);

  UFUNCTION(BlueprintCallable)
  bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);

};
