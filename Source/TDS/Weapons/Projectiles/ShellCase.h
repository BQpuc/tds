// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS/FuncLibrary/Types.h"
#include "ShellCase.generated.h"

UCLASS()
class TDS_API AShellCase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShellCase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* Mesh = nullptr;






	//   VARIABLES:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FDropMeshInfo DropInfo;

	float DestroyTimer = 0.0f;



	//   FUNCTIONS:
	UFUNCTION()
		void ShellInit(FDropMeshInfo DropInformation, FVector LocalDir);

	void DestroyTick(float DeltaTime);

};
