// Fill out your copyright notice in the Description page of Project Settings.


#include "ShellCase.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/MeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
AShellCase::AShellCase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShellCase"));
	Mesh->SetGenerateOverlapEvents(false);
	Mesh->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
	Mesh->SetupAttachment(RootComponent);
	
}

// Called when the game starts or when spawned
void AShellCase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AShellCase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DestroyTick(DeltaTime);
}


void AShellCase::ShellInit(FDropMeshInfo DropInformation, FVector LocalDir)
{
	// ��������� ���������� DropMesh �� �������
	DropInfo = DropInformation;
	// ��������� ����� ������/������
	DestroyTimer = DropInfo.DropMeshLifeTime;
	// ������������ StaticMesh �� �������
	if (DropInfo.DropMesh)
	{
		Mesh->SetStaticMesh(DropInfo.DropMesh);
		Mesh->SetRelativeTransform(DropInfo.DropMeshOffset);
	}
	Mesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	// ������� ������
	Mesh->Mobility = EComponentMobility::Movable;
	Mesh->SetSimulatePhysics(true);
	// ������������� ��������� �������� ������� �������
	Mesh->SetCollisionResponseToChannel(ECC_EngineTraceChannel1 , ECollisionResponse::ECR_Ignore);
	Mesh->SetCollisionResponseToChannel(ECC_EngineTraceChannel2, ECollisionResponse::ECR_Ignore);
	Mesh->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
	Mesh->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
	Mesh->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
	Mesh->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);
	// ������������� ����� ����
	if (DropInfo.CustomMass > 0.0f)
	{
		Mesh->SetMassOverrideInKg(NAME_None, DropInfo.CustomMass);
	}
	FVector FinalDir;
	FVector LocalDirect = LocalDir;
	if (!FMath::IsNearlyZero(DropInfo.ImpulseRandomDispersion))
	{
		FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDirect,
		DropInfo.ImpulseRandomDispersion);
	}
	FinalDir.GetSafeNormal(0.01f);
	float Impulse = UKismetMathLibrary::RandomFloatInRange
	(DropInfo.PowerImpulse - DropInfo.RandomPowerImpulse,
		DropInfo.PowerImpulse + DropInfo.RandomPowerImpulse);
	Mesh->AddImpulse(FinalDir * Impulse);
}

// ����������� ������ ����� ������ ����� ������ ����
void AShellCase::DestroyTick(float DeltaTime)
{
	if (DropInfo.DropMeshLifeTime > 0.0f)
	{
		if (DestroyTimer < 0.0f)
		{
			this->Destroy();
		}
		else
		{
			DestroyTimer -= DeltaTime;
		}
	}
}