// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	BulletCollisionSphere->SetSphereRadius(16.f);



	BulletCollisionSphere->bReturnMaterialOnMove = true;//hit event return physMaterial

	BulletCollisionSphere->SetCanEverAffectNavigation(false);//collision not affect navigation (P keybord on editor)

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	//BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
	//BulletSound->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 2500.f;
	BulletProjectileMovement->MaxSpeed = 2500.f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();

	BulletCollisionSphere->OnComponentHit.AddDynamic
	(this, &AProjectileDefault::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic
	(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic
	(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
	BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	BulletProjectileMovement->MaxSpeed = InitParam.ProjectileInitSpeed;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);
	if (InitParam.ProjectileStaticMesh)
	{
		BulletMesh->SetStaticMesh(InitParam.ProjectileStaticMesh);
		BulletMesh->SetRelativeTransform(InitParam.ProjectileStaticMeshffset);
	}
	else
	{
		BulletMesh->DestroyComponent();
	}
	if (InitParam.ProjectileTrailFx)
	{
		BulletFX->SetTemplate(InitParam.ProjectileTrailFx);
		BulletFX->SetRelativeTransform(InitParam.ProjectileTrailFxOffset);
	}
	else
	{
		BulletMesh->DestroyComponent();
	}
	ProjectileSetting = InitParam;
}

void AProjectileDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, 
	AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, 
	const FHitResult& Hit)
{
	if (OtherActor)//(OtherActor && Hit.PhysMaterial.IsValid())
	{
		bool PhMat = Hit.PhysMaterial.IsValid();
		if (PhMat)
		{
			EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);
			bool bDecals = ProjectileSetting.HitDecals.Contains(mySurfacetype);
			bool bFXs = ProjectileSetting.HitFXs.Contains(mySurfacetype);
			bool bSound = IsValid(ProjectileSetting.HitSound);
			if (bDecals)//(ProjectileSetting.HitDecals.Contains(mySurfacetype))
			{
				UMaterialInterface* myMaterial = ProjectileSetting.HitDecals[mySurfacetype];
				if (myMaterial && OtherActor)
				{
					UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f),
						OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(),
						EAttachLocation::KeepWorldPosition, 10.0f);
				}
			}
			if (bFXs)//(ProjectileSetting.HitFXs.Contains(mySurfacetype))
			{
				UParticleSystem* myParticle = ProjectileSetting.HitFXs[mySurfacetype];
				if (myParticle)
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle,
						FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint,
							FVector(1.0f)));
				}
			}
			if (bSound)//(ProjectileSetting.HitSound)
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.HitSound,
					Hit.ImpactPoint);
			}

		      UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileSetting.Effect, mySurfacetype);
		}
	}



	UGameplayStatics::ApplyDamage(OtherActor, ProjectileSetting.ProjectileDamage, 
		GetInstigatorController(), this, NULL);
	ImpactProjectile();
}

void AProjectileDefault::BulletCollisionSphereBeginOverlap
(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, 
	UPrimitiveComponent* otherComp, int32 OtherBodyIndex, bool bFromSweep, 
	const FHitResult& SweepResult)
{
}

void AProjectileDefault::BulletCollisionSphereEndOverlap
(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}