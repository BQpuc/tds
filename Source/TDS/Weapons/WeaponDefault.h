// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDS/FuncLibrary/TDS_StateEffect.h"
#include "WeaponDefault.generated.h"

class USceneComponent;
class USceletalMeshComponent;
class UStaticMeshComponent;
class UArrowComponent;
class AStaticMeshActor;
class AProjectileDefault;
class AShellCase;
class UInventotyComponent;
class UAnimMontage;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(
    FOnWeaponFireStart, UAnimMontage*, Anim); // ToDo Delegate on event weapon fire - Anim char, state char...
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);

UCLASS()
class TDS_API AWeaponDefault : public AActor
{
  GENERATED_BODY()

  public:
  // Sets default values for this actor's properties
  AWeaponDefault();

  // Called every frame
  virtual void Tick(float DeltaTime) override;

  FOnWeaponFireStart OnWeaponFireStart;
  FOnWeaponReloadStart OnWeaponReloadStart;
  FOnWeaponReloadEnd OnWeaponReloadEnd;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
  class USceneComponent* SceneComponent = nullptr;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
  class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
  class UStaticMeshComponent* StaticMeshWeapon = nullptr;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
  class UArrowComponent* ShootLocation = nullptr;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
  class UArrowComponent* MagLocation = nullptr;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
  class UArrowComponent* ShellLocation = nullptr;

  protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  public:
  // WARIEBLES
  UPROPERTY(VisibleAnywhere)
  FWeaponInfo WeaponSetting;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
  FAdditionalWeaponInfo AdditionalWeaponInfo;

  // flags
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
  bool WeaponFiring = false;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
  // ��������� �� ������ � �������� �����������
  bool WeaponReloading = false;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Anim")
  bool WeaponAiming = false;

  // Timers
  float FireTimer = 0.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
  float ReloadTimer = 0.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
  float ReloadTime = 0.0f;

  // flags
  bool BlockFire = false;

  // Dispersion
  bool ShouldReduceDispersion = false;
  float CurrentDispersion = 0.0f;
  float CurrentDispersionMax = 1.0f;
  float CurrentDispersionMin = 0.1f;
  float CurrentDispersionRecoil = 0.1f;
  float CurrentDispersionReduction = 0.1f;

  FVector ShootEndLocation = FVector(0);

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
  bool ShowDebug = false;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
  float SizeVectorToChangeShootDirectionLogic = 100.0f;

  // Timer Drop Magazine on reload
  bool DropClipFlag = false;
  float DropClipTimer = -1.0;

  // Shell Flag
  bool DropShellFlag = false;
  float DropShellTimer = -1.0f;

  UPROPERTY(BlueprintReadWrite)
  FName IdWeaponName = FName("None");

  // FUNCTIONS
  // Tick func
  void FireTick(float Deltatime);
  void ReloadTick(float DeltaTime);
  void DispersionTick(float DeltaTime);
  void ClipDropTick(float DeltaTime);
  void ShellDropTick(float DeltaTime);

  UFUNCTION()
  void WeaponInit();

  UFUNCTION(BlueprintCallable)
  void SetWeaponStateFire(bool bIsFire);
  UFUNCTION()
  bool CheckWeaponCanFire();

  UFUNCTION()
  FProjectileInfo GetProjectile();
  UFUNCTION()
  void Fire();

  void UpdateStateWeapon(EMovementState NewMovementState);
  void ChangeDispersionByShot();
  float GetCurrentDispersion() const;
  FVector ApplyDispersionToShot(FVector DirectionShoot) const;

  FVector GetFireEndLocation() const;
  int8 GetNumberProjectileByShot() const;

  UFUNCTION(BlueprintCallable)
  // KolichecTBo ||aTPoHoB B Maga3uHe
  int32 GetWeaponRound();

  UFUNCTION()
  void InitReload();
  // ����������� �������� ����������� � ������ ����� ��������. �������� false ������� ��������� �����������
  void CancelReload();
  // ��������� ������� � �������, �������� true ������� ��������� �����������
  void FinishReload();

  bool CheckCanWeaponReload();
  int8 GetAviableAmmoForReload();

  /*UFUNCTION()
  void InitDropMesh(FVector SpLoc, FVector Dir, TSubclassOf<AShellCase> Actor, FDropMeshInfo Info);
  */
  UFUNCTION()
  void InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh,
      float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);
};
