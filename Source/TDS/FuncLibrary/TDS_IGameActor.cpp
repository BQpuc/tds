// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_IGameActor.h"
#include "TDS/FuncLibrary/TDS_StateEffect.h"

// Add default functionality here for any ITDS_IGameActor functions that are not pure virtual.

TEnumAsByte<EPhysicalSurface> ITDS_IGameActor::GetSurfaceType()
{
  return EPhysicalSurface::SurfaceType_Default;
}

TArray<UTDS_StateEffect*> ITDS_IGameActor::GetAllCurrentEffects()
{
  return TArray<UTDS_StateEffect*>();
}

void ITDS_IGameActor::RemoveEffect(UTDS_StateEffect* RemoveEffect) {}

void ITDS_IGameActor::AddEffect(UTDS_StateEffect* newEffect) {}

//void ITDS_IGameActor::DropWeaponToWorld(FDropItem DropItemInfo) {}

//void ITDS_IGameActor::DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout) {}
