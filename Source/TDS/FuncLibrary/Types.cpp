#include "Types.h"
#include "TDS/TDS.h"
#include "TDS/Weapons/Projectiles/ProjectileDefault.h"
#include "TDS/Weapons/Projectiles/ShellCase.h"
#include "TDS/FuncLibrary/TDS_StateEffect.h"
#include "TDS/FuncLibrary/TDS_IGameActor.h"

DEFINE_LOG_CATEGORY_STATIC(LogTypes, All, All)

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<class UTDS_StateEffect> AddEffectClass, 
	TEnumAsByte<EPhysicalSurface> SurfaceType) 
{
    // проверки
  if (!TakeEffectActor)
  {
    UE_LOG(LogTypes, Error, TEXT("TakeEffectActor: %s"), *TakeEffectActor->GetName());
    return;
  }
  if (SurfaceType == EPhysicalSurface::SurfaceType_Default)
  {
    UE_LOG(LogTypes, Warning, TEXT("SurfaceType is Empty, %s"), *TakeEffectActor->GetName());
    return;
  }
  if (!AddEffectClass)
  {
    UE_LOG(LogTypes, Error, TEXT("TakeEffectActor: None. Settins the effect in the DataTable"));
    return;
  }
   

  UTDS_StateEffect* myEffect = Cast<UTDS_StateEffect>(AddEffectClass->GetDefaultObject());
  if (!myEffect)   
  {
    UE_LOG(LogTypes, Warning, TEXT("Cast<UTDS_StateEffect>"));
    return;
  }
  
  bool bIsCanAdd = false;
  
  for (int32 i = 0; i < myEffect->PossibleInteractSurface.Num() && !bIsCanAdd; i++)
  {
    if (myEffect->PossibleInteractSurface[i] == SurfaceType)
    {
      bIsCanAdd = true;
      bool bIsCanAddEffect = false;
      if (!myEffect->bIsStakable)
      {
        TArray<UTDS_StateEffect*> CurrentEffects;
        ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(TakeEffectActor);
        if (myInterface)
          CurrentEffects = myInterface->GetAllCurrentEffects();

        if (CurrentEffects.Num() > 0)
        {
          for (int32 j = 0; j < CurrentEffects.Num() && !bIsCanAddEffect; j++)
          {
            if (CurrentEffects[j]->GetClass() != AddEffectClass)
              bIsCanAddEffect = true;
          }
        }
        else
        {
          bIsCanAddEffect = true;
        }
      }
      else
      {
        bIsCanAddEffect = true;
      }

      if (bIsCanAddEffect)
      {
        UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(TakeEffectActor, AddEffectClass);
        if (NewEffect)
        {
          NewEffect->InitObject(TakeEffectActor);
          UE_LOG(LogTypes, Display, TEXT("Class: %s buff/debuf"), *TakeEffectActor->GetName());
        }
      }
    }
  }
  if (!bIsCanAdd)
  {
    UE_LOG(LogTypes, Warning, TEXT("Class: %s not interact with Physical material"), *AddEffectClass->GetName());
  }
}