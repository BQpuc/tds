// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TDS_StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TDS_API UTDS_StateEffect : public UObject
{
	GENERATED_BODY()
	
public:
  virtual bool InitObject(AActor* Actor);
  virtual void ExecuteEffect(float DeltaTime);
  virtual void DestroyObject();

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
  TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
  bool bIsStakable = false;
  UPROPERTY()
  AActor* myActor = nullptr;
};

UCLASS(Blueprintable, BlueprintType)
class TDS_API UTDS_StateEffect_ExecuteOnce : public UTDS_StateEffect
{
  GENERATED_BODY()

public:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
  float Power = 20.f;

  bool InitObject(AActor* Actor) override;
  void DestroyObject() override;

  virtual void ExecuteOnce();
};

UCLASS(Blueprintable, BlueprintType)
class TDS_API UTDS_StateEffect_ExecuteTimer : public UTDS_StateEffect
{
  GENERATED_BODY()

  public:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
  float Power = 20.f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
  float Timer = 5.f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
  float RateTime = 1.f;
  FTimerHandle TimerHandle_ExecuteTimer;
  FTimerHandle TimerHandle_EffectTimer;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
  UParticleSystem* ParticleEffect = nullptr;
  UPROPERTY()
  UParticleSystemComponent* ParticleEmitter = nullptr;

  bool InitObject(AActor* Actor) override;
  void DestroyObject() override;

  virtual void Execute();
};