// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_StateEffect.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/Character/TDS_HealthComponent.h"
#include "TDS/FuncLibrary/TDS_IGameActor.h"

bool UTDS_StateEffect::InitObject(AActor* Actor)
{
  myActor = Actor;

  ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
  if (myInterface)
  {
    myInterface->AddEffect(this);
  }
  return true;
}
void UTDS_StateEffect::ExecuteEffect(float DeltaTime)
{
  UE_LOG(LogTemp, Display, TEXT("UTDS_StateEffect::ExecuteEffect"));
}

void UTDS_StateEffect::DestroyObject()
{
  ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
  if (myInterface)
  {
    myInterface->RemoveEffect(this);
  }

  myActor = nullptr;
  if (this && this->IsValidLowLevel())
  {
    this->ConditionalBeginDestroy();
  }
}

bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
  Super::InitObject(Actor);
  ExecuteOnce();
  return false;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
  Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
  if (myActor)
  {
    UTDS_HealthComponent* myHealthComp = Cast<UTDS_HealthComponent>(
        myActor->GetComponentByClass(UTDS_HealthComponent::StaticClass()));
    if (myHealthComp)
    {
      myHealthComp->ChangeHealthValue(Power);
    }
  }

  DestroyObject();
}

bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
  Super::InitObject(Actor);

  GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, 
      this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
  GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, 
      this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);

  if (ParticleEffect)
  {
    FName NameBoneToAttached;
    FVector Loc = FVector(0);

    ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(
        ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, 
        FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
  }

  return true;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
  ParticleEmitter->DestroyComponent();
  ParticleEmitter = nullptr;
  Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
  if (myActor)
  {
    // UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);
    UTDS_HealthComponent* myHealthComp = Cast<UTDS_HealthComponent>(
        myActor->GetComponentByClass(UTDS_HealthComponent::StaticClass()));
    if (myHealthComp)
    {
      myHealthComp->ChangeHealthValue(Power);
    }
  }
}


