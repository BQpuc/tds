// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
  Aim_State UMETA(DisplayName = "Aim State"),
  AimWalk_State UMETA(DisplayName = "AimWalk State"),
  Walk_State UMETA(DisplayName = "Walk State"),
  RunState UMETA(DisplayName = "Run State"),
  Sprint_State UMETA(DisplayName = "Sprint State"),
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
  RifleType UMETA(DisplayName = "Rifle"),
  ShotGunType UMETA(DisplayName = "ShotGun"),
  SniperRifle UMETA(DisplayName = "SniperRifle"),
  //GrenadeLauncher UMETA(DisplayName = "GrenadeLauncher"),
  RocketLauncher UMETA(DisplayName = "RocketLauncher")
};

USTRUCT(BlueprintType)
//float : AimSpeed, WalkSpeed, RunSpeed, AimSpeedWalk, SprintSpeed
struct FCharacterSpeed
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
  float AimSpeed = 400.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
  float WalkSpeed = 250.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
  float RunSpeed = 600.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
  float AimSpeedWalk = 250.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
  float SprintSpeed = 800.0f;
};

USTRUCT(BlueprintType)
/*float: ProjectileDamage, ProjectileLifeTime, ProjectileInitSpeed, ProjectileMinRadiusDamage, ProjectileMaxRadiusDamage, ExploseMaxDamage, ExploseFalloffCoef/ 
FTransform: ProjectileStaticMeshffset, ProjectileTrailFxOffset/ TSubclassOf<class AProjectileDefault> Projectile, UStaticMesh* ProjectileStaticMesh,
 TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals, USoundBase* HitSound, TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs, 
 TSubclassOf<class UTDS_StateEffect> Effect, UParticleSystem* ExploseFX, USoundBase* ExploseSound */
struct FProjectileInfo
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
  TSubclassOf<class AProjectileDefault> Projectile = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
  UStaticMesh* ProjectileStaticMesh = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
  FTransform ProjectileStaticMeshffset = FTransform();
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
  UParticleSystem* ProjectileTrailFx = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
  FTransform ProjectileTrailFxOffset = FTransform();
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings", meta = (ClampMin = "0.0", ClampMax = "999999.9"))
  float ProjectileDamage = 20.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
  float ProjectileLifeTime = 20.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
  float ProjectileInitSpeed = 2000.0f;

  // material to decal on hit
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
  TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
  // Sound when hit
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
  USoundBase* HitSound = nullptr;
  // Fx when hit check by surface
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
  TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
  TSubclassOf<class UTDS_StateEffect> Effect = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explose")
  UParticleSystem* ExploseFX = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explose")
  USoundBase* ExploseSound = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explose")
  float ProjectileMinRadiusDamage = 30.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explose")
  float ProjectileMaxRadiusDamage = 200.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explose")
  float ExploseMaxDamage = 40.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explose")
  float ExploseFalloffCoef = 1.0f;
};

USTRUCT(BlueprintType)
// float: Aim_StateDispersionAimMax, Aim_StateDispersionAimMin, Aim_StateDispersionAimRecoil, Aim_StateDispersionReduction, AimWalk_StateDispersionAimMax, AimWalk_StateDispersionAimMin...
struct FWeaponDispersion
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Aim_StateDispersionAimMax = 2.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Aim_StateDispersionAimMin = 0.3f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Aim_StateDispersionAimRecoil = 1.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Aim_StateDispersionReduction = .3f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float AimWalk_StateDispersionAimMax = 1.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float AimWalk_StateDispersionAimMin = 0.1f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float AimWalk_StateDispersionAimRecoil = 1.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float AimWalk_StateDispersionReduction = 0.4f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Walk_StateDispersionAimMax = 5.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Walk_StateDispersionAimMin = 1.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Walk_StateDispersionAimRecoil = 1.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Walk_StateDispersionReduction = 0.2f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Run_StateDispersionAimMax = 10.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Run_StateDispersionAimMin = 4.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Run_StateDispersionAimRecoil = 1.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
  float Run_StateDispersionReduction = 0.1f;
};

USTRUCT(BlueprintType)
// UAnimMontage*: AnimCharFire, AnimCharFireAim, AnimCharReload, AnimCharReloadAim, AnimWeaponReload, AnimWeaponReloadAim, AnimWeaponFire/ float: DisplHigh, DispAimHigh
struct FAnimationWeaponInfo
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
  UAnimMontage* AnimCharFire = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
  UAnimMontage* AnimCharFireAim = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
  UAnimMontage* AnimCharReload = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
  UAnimMontage* AnimCharReloadAim = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimWeapon")
  UAnimMontage* AnimWeaponReload = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimWeapon")
  UAnimMontage* AnimWeaponReloadAim = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimWeapon")
  UAnimMontage* AnimWeaponFire = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
  float DisplHigh = 125.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
  float DispAimHigh = 165.0f;
};

USTRUCT(BlueprintType)
// TSubclassOf<class AShellCase> DropActor/ UStaticMesh* DropMesh/ FTransform DropMeshOffset/ float: PowerImpulse, DropMeshTime, RandomPowerImpulse, ImpulseRandomDispersion, CustomMass/ FVector DropMeshImpulseDir
struct FDropMeshInfo
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
  TSubclassOf<class AShellCase> DropActor = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
  UStaticMesh* DropMesh = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
  float DropMeshTime = -1.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
  float DropMeshLifeTime = 5.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
  FTransform DropMeshOffset = FTransform();
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
  float PowerImpulse = 0.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
  float RandomPowerImpulse = 0.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
  float ImpulseRandomDispersion = 0.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
  float CustomMass = 0.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
  FVector DropMeshImpulseDir = FVector(0.0f);
};


USTRUCT(BlueprintType)
/* TSubclassOf<class AWeaponDefault> WeaponClass/ float: RateOfFire, ReloadTime, DistanceTrace, SwitchTimeToWeapon/ int32: MaxRound, 
NumberProjectileByShot/ FWeaponDispersion DispersionWeapon/ USoundBase*: SoundFireWeapon, 
SoundReloadWeapon/ UParticleSystem* EffectFireWeapon/ FProjectileInfo ProjectileSettings/ 
 UDecalComponent* DecalOnHit/ FAnimationWeaponInfo AnimWeaponInfo/ FDropMeshInfo: ClipDropMesh,
ShellBullets/ UTexture2D* WeaponIcon/ EWeaponType WeaponType
*/
struct FWeaponInfo : public FTableRowBase
{
  GENERATED_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
  TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
  float RateOfFire = 0.5f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
  float ReloadTime = 2.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
  int32 MaxRound = 10;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
  int32 NumberProjectileByShot = 1;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
  FWeaponDispersion DispersionWeapon;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
  USoundBase* SoundFireWeapon = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
  USoundBase* SoundReloadWeapon = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
  UParticleSystem* EffectFireWeapon = nullptr;
  // if null use trace logic (TSubclassOf<AProjectileDefault> Projectile = nullptr)
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
  FProjectileInfo ProjectileSettings;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
  float DistanceTrace = 2000.0f;
  // one decal on all?
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect")
  UDecalComponent* DecalOnHit = nullptr;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
  FAnimationWeaponInfo AnimWeaponInfo;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
  FDropMeshInfo ClipDropMesh;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
  FDropMeshInfo ShellBullets;

  // inv
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
  float SwitchTimeToWeapon = 1.0f;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
  UTexture2D* WeaponIcon = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
  EWeaponType WeaponType = EWeaponType::RifleType;
};

USTRUCT(BlueprintType)
// int32 Round
struct FAdditionalWeaponInfo
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
  int32 Round = 0;
};

USTRUCT(BlueprintType)
// FName NameItem, FAdditionalWeaponInfo AdditionalInfo
struct FWeaponSlot
{
  GENERATED_USTRUCT_BODY()

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
  // �������� ������
  FName NameItem = "None";

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
  // ��������� � ������� ����������� �������� � ��������
  FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
// EWeaponType WeaponType/ int32: Cout, MaxCout
struct FAmmoSlot
{
  GENERATED_USTRUCT_BODY()

  /// Index Slot by Index Array
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
  // ���, � �������� ������ �������� �������
  EWeaponType WeaponType = EWeaponType::RifleType;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
  // ������� ���������� ��������
  int32 Cout = 100;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
  // ������������ ���������� ��������
  int32 MaxCout = 100;
};

USTRUCT(BlueprintType)
// UStaticMesh* WeaponStaticMesh, USkeletalMesh* WeaponSkeletMesh, FWeaponSlot AdditionalWeaponInfo
struct FDropItem : public FTableRowBase
{
  GENERATED_USTRUCT_BODY()

  /// Index Slot by Index Array
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
  UStaticMesh* WeaponStaticMesh = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
  USkeletalMesh* WeaponSkeletMesh = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
  FWeaponSlot AdditionalWeaponInfo;
};

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
  GENERATED_BODY()

	  public:
  UFUNCTION()
  static void AddEffectBySurfaceType(AActor* TakeEffectActor, 
	TSubclassOf<class UTDS_StateEffect> AddEffectClass, TEnumAsByte<EPhysicalSurface> SurfaceType);
};