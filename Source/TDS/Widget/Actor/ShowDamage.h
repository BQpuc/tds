// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShowDamage.generated.h"

class UWidgetComponent;

UCLASS()
class TDS_API AShowDamage : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShowDamage();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

//private:
 // UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
 // class UWidgetComponent* MyWidget;

  public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//FORCEINLINE class UWidgetComponent* GetWidgetComponent() const { return MyWidget; }

	UFUNCTION(BlueprintNativeEvent)
  void SpawnDamageWidget(float Damage, FLinearColor Color);
  void SpawnDamageWidget_Implementation(float Damage, FLinearColor Color);
};
