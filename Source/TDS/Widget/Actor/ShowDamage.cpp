// Fill out your copyright notice in the Description page of Project Settings.


#include "ShowDamage.h"
#include "Components/WidgetComponent.h"

// Sets default values
AShowDamage::AShowDamage()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//MyWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget"));
    //MyWidget->SetupAttachment(GetRootComponent());
}

// Called when the game starts or when spawned
void AShowDamage::BeginPlay()
{
	Super::BeginPlay();
	
	SetLifeSpan(1.f);
}

// Called every frame
void AShowDamage::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AShowDamage::SpawnDamageWidget_Implementation(float Damage, FLinearColor Color) {}

