// Fill out your copyright notice in the Description page of Project Settings.


#include "WorldItemDefault.h"
#include "Components/SphereComponent.h"
#include "TDS/Character/TDSCharacter.h"
#include "TDS/Character/InventoryComponent.h"

// Sets default values
AWorldItemDefault::AWorldItemDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
  SceneComp = CreateDefaultSubobject<USceneComponent>("Scene");
  SetRootComponent(SceneComp);

  SphereCollision = CreateDefaultSubobject<USphereComponent>("Sphere");
  SphereCollision->SetupAttachment(GetRootComponent());
  
}

// Called when the game starts or when spawned
void AWorldItemDefault::BeginPlay()
{
	Super::BeginPlay();

  if (SphereCollision)
    SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &AWorldItemDefault::SphereBeginOverlap);
}

void AWorldItemDefault::SphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
    int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
  if (!OtherActor)
    return;
  ATDSCharacter* Player = Cast<ATDSCharacter>(OtherActor);
  if (!Player)
    return;
  OverlapCharacter(Player->InventoryComponent);
}

void AWorldItemDefault::OverlapCharacter_Implementation(UInventoryComponent* Inventory) {}

// Called every frame
void AWorldItemDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWorldItemDefault::InitActor_Implementation(FDropItem InfoActor) {}

void AWorldItemDefault::PickUpSuccess_Implementation()
{
  Destroy();
}

