// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyActor.generated.h"

UENUM(BlueprintType)
enum class EStand : uint8
{
	Normal_Stand   	UMETA(DisplayName = "Normal Stand"),
	Small_Stand		 UMETA(DisplayName = "Small Stand"),
	Table_Stand		 UMETA(DisplayName = "Table Stand"),
	TwoSmall_Stand_1	UMETA(DisplayName = "2 Small Stand"),
	Barrel_Stand_2	UMETA(DisplayName = "Barrel Stand"),
	
	None			UMETA(DisplayName = "None"),
};

UENUM(BlueprintType)
enum class ECover : uint8
{
	Cover_1			UMETA(DisplayName = "Cover 1"),
	Cover_2			 UMETA(DisplayName = "Cover 2"),
	Cover_3			UMETA(DisplayName = "Cover 3"),
	CoverDestroyed	UMETA(DisplayName = "Cover Destroyed"),
	CoverSmall		UMETA(DisplayName = "Cover Small"),
	None			 UMETA(DisplayName = "None"),
};

UENUM(BlueprintType)
enum class EOption : uint8
{
	Apples			UMETA(DisplayName = "Apples"),
	Fish			UMETA(DisplayName = "Fishes"),
	Tomatoes		UMETA(DisplayName = "Tomatoes"),
	Potatoes		UMETA(DisplayName = "Potatoes"),
	Oyster			UMETA(DisplayName = "Oyster"),
	None		 	 UMETA(DisplayName = "None"),
};

UCLASS()
class TDS_API AMyActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Market")
		EStand Stand = EStand::Normal_Stand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Market")
		ECover Cover = ECover::Cover_1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MArket")
		EOption OptionLeft = EOption::None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MArket")
		EOption OptionRight = EOption::None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MArket")
		EOption OptionLover = EOption::None;
};
