// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_DestructionBase.h"
#include "TDS/Character/TDS_HealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TDS/Widget/Actor/ShowDamage.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TDS/Weapons/Projectiles/ProjectileDefault.h"


// Sets default values
ATDS_DestructionBase::ATDS_DestructionBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	HealthComponent = CreateDefaultSubobject<UTDS_HealthComponent>(TEXT("HealthComponent"));
  if (HealthComponent)
  {
    HealthComponent->OnHealthChange.AddDynamic(this, &ATDS_DestructionBase::HealthChange);
    HealthComponent->OnDead.AddDynamic(this, &ATDS_DestructionBase::Dead);
  }
  OnTakeAnyDamage.AddDynamic(this, &ATDS_DestructionBase::DB_AnyDamage);
  OnTakeRadialDamage.AddDynamic(this, &ATDS_DestructionBase::DB_RadialDamage);
}

// Called when the game starts or when spawned
void ATDS_DestructionBase::BeginPlay()
{
	Super::BeginPlay();

  check(GetWorld());
}

void ATDS_DestructionBase::HealthChange(float Health, float Damage) 
{
  if (!WidgetActor)
    return;

	// ����������� ����� �������
  FLinearColor NewColor = NormalColor;
  if (Health < HealthComponent->MaxHealth * ChangeColorPercent)
    NewColor = LowHPColor;

  // ����� ������, ����������� ������
  FVector Point = SpanwWidgetPoint + GetActorLocation() +
      FVector(UKismetMathLibrary::RandomFloatInRange(-RandLoc, RandLoc),
		  UKismetMathLibrary::RandomFloatInRange(-RandLoc, RandLoc), 0.f);
  FRotator ActorRotation = FRotator();
  FActorSpawnParameters SpParam;
  SpParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
  AShowDamage* Widget = Cast<AShowDamage>(GetWorld()->SpawnActor(WidgetActor, &Point, &ActorRotation, SpParam));
  if(Widget)
	  Widget->SpawnDamageWidget(Damage, NewColor);
}

void ATDS_DestructionBase::Dead() 
{
  if (DeathSound)
  UGameplayStatics::SpawnSoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
  if (DeathFX)
  UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DeathFX, GetActorLocation());

  this->Destroy();
}

void ATDS_DestructionBase::DB_AnyDamage(
    AActor* DamagedActor, 
	float Damage, 
	const UDamageType* DamageType, 
	AController* InstigatedBy, 
	AActor* DamageCauser)
{
  //HealthComponent->ChangeHealthValue(-Damage);
  if (DamagedActor){
    UE_LOG(LogTemp, Display, TEXT("%s damaged %f.1"), *DamagedActor->GetName(), Damage);
  }
}

void ATDS_DestructionBase::DB_RadialDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, FVector Origin,
    FHitResult HitInfo, AController* InstigatedBy, AActor* DamageCauser)
{
  AProjectileDefault* Causer = Cast<AProjectileDefault>(DamageCauser);
  if (!Causer)
  {
    if (DamageCauser)
    {
      UE_LOG(LogTemp, Error, TEXT("Radial Damage: Causer = %s"), *DamageCauser->GetName());
    }
    else 
    { 
      UE_LOG(LogTemp, Error, TEXT("Radial Damage: Causer is none"));
    }
    return;
  }
   UE_LOG(LogTemp , Warning, TEXT("Radial Damage: DestructionBase"));
 
  UTypes::AddEffectBySurfaceType(this, Causer->ProjectileSetting.Effect, GetSurfaceType());
}

float ATDS_DestructionBase::TakeDamage(
    float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
  float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
  if (HealthComponent->GetCurrentHealth() > 0)
    HealthComponent->ChangeHealthValue(-DamageAmount);

  if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
  {
    AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
    if (myProjectile)
    {
      UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
    }
  }

  return ActualDamage;
}

TEnumAsByte<EPhysicalSurface> ATDS_DestructionBase::GetSurfaceType()
{
  EPhysicalSurface result = EPhysicalSurface::SurfaceType_Default;
  UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
  if (myMesh && myMesh->GetMaterial(0))
  {
    UPhysicalMaterial* myPMat = myMesh->GetMaterial(0)->GetPhysicalMaterial();
    if (myPMat)
    {
      result = myPMat->SurfaceType;
    }
  }
  return result;
}

TArray<UTDS_StateEffect*> ATDS_DestructionBase::GetAllCurrentEffects()
{
  return Effects;
}

void ATDS_DestructionBase::RemoveEffect(UTDS_StateEffect* RemoveEffect)
{
  Effects.Remove(RemoveEffect);
}

void ATDS_DestructionBase::AddEffect(UTDS_StateEffect* newEffect)
{
  Effects.Add(newEffect);
}
