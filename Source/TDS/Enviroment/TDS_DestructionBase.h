// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS/FuncLibrary/TDS_IGameActor.h"
#include "TDS_DestructionBase.generated.h"

class UTDS_HealthComponent;
class AShowDamage;

UCLASS()
class TDS_API ATDS_DestructionBase : public AActor, public ITDS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_DestructionBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = true))
	UTDS_HealthComponent* HealthComponent = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	USoundBase* DeathSound = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
  UParticleSystem* DeathFX = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
  FVector SpanwWidgetPoint = FVector();
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
  float RandLoc = 200.f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
  FLinearColor LowHPColor = FLinearColor::Red;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
  FLinearColor NormalColor = FLinearColor::White;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect", meta = (ClampMin = "0", ClampMax = "1"))
  float ChangeColorPercent = 0.5f;

  public:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
  TSubclassOf<class AShowDamage> WidgetActor = nullptr;

  	// Effect
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
  TArray<UTDS_StateEffect*> Effects;

  //  ---Interface---
  TEnumAsByte<EPhysicalSurface> GetSurfaceType() override;
  TArray<UTDS_StateEffect*> GetAllCurrentEffects() override;
  void RemoveEffect(UTDS_StateEffect* RemoveEffect) override;
  void AddEffect(UTDS_StateEffect* newEffect) override;
  //  ---End interface---

  protected:
  UFUNCTION()
  virtual void HealthChange(float Health, float Damage);
  UFUNCTION()
  virtual void Dead();
  UFUNCTION()
  void DB_AnyDamage(
      AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
  UFUNCTION()
  void DB_RadialDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, FHitResult HitInfo,
      class AController* InstigatedBy, AActor* DamageCauser);

  virtual float TakeDamage(
      float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
};
